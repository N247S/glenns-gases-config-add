# README #

This is Glenn's Gases. It adds gases among other things to Minecraft using the [Gases Framework](https://bitbucket.org/jamieswhiteshirt/gases-framework).

**To be able to run and develop Glenn's Gases in your workspace using Eclipse, you must follow these steps:**

* Download a forge distribution and copy the eclipse folder into this workspace.
* Run *gradlew setupDevWorkspace* in your workspace.
* Run *gradlew eclipse* in your workspace.
* Open the workspace with Eclipse.
* Copy a version of the latest [developer build](google.com) of Gases Framework and install it to /eclipse/mods/ .
* Open your run configurations and add the following VM argument to both Client and Server: *-Dfml.coreMods.load=glenn.gases.common.core.GGFMLLoadingPlugin*