package glenn.gases.common.item;

import glenn.gases.common.entity.EntityDust;

import java.lang.reflect.Constructor;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemDust extends Item
{
	private Constructor entityConstructor;
	
	public ItemDust(Class<? extends EntityDust> entityClass)
	{
		Constructor[] constructors = entityClass.getConstructors();
		
		for(Constructor constructor : constructors)
		{
			Class<?>[] params = constructor.getParameterTypes();
			if(params.length == 2 && params[0] == World.class && params[1] == EntityLivingBase.class)
			{
				entityConstructor = constructor;
				return;
			}
		}
	}
	
	/**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
	@Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if(!par3EntityPlayer.capabilities.isCreativeMode)
        {
            --par1ItemStack.stackSize;
        }

        par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if(!par2World.isRemote)
        {
        	for(int i = 0; i < 50; i++)
        	{
        		par2World.spawnEntityInWorld(getThrownEntity(par2World, par3EntityPlayer));
        	}
        }

        return par1ItemStack;
    }
	
	public EntityDust getThrownEntity(World world, EntityPlayer player)
	{
		try
		{
			return (EntityDust)entityConstructor.newInstance(world, player);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
}