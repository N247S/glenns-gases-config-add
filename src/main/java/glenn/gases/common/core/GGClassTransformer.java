package glenn.gases.common.core;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IF_ACMPEQ;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INSTANCEOF;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.LDC;
import static org.objectweb.asm.Opcodes.POP;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.launchwrapper.IClassTransformer;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

import cpw.mods.fml.common.FMLLog;

public class GGClassTransformer implements IClassTransformer
{
	private Map<String, String> c = new HashMap<String, String>();
	
	{
		c.put("Block", "aji");
		c.put("Blocks", "ajn");
		c.put("BlockFire", "alb");
		c.put("ItemRenderer", "bly");
		c.put("Entity", "sa");
		c.put("EntityLivingBase", "sv");
		c.put("ItemGlassBottle", "abl");
		c.put("BlockLiquid", "alw");
		c.put("BlockDynamicLiquid", "akr");
		c.put("EntityRenderer", "blt");
		c.put("WorldProvider", "aqo");
		c.put("World", "ahb");
		c.put("Material", "awt");
		c.put("MaterialLiquid", "aws");
		c.put("EntityPlayer", "yz");
		c.put("MovingObjectPosition", "azu");
		c.put("ItemStack", "add");
		c.put("Item", "adb");
		c.put("InventoryPlayer", "yx");
		c.put("ItemPotion", "adp");
		c.put("EntityItem", "xk");
		c.put("MathHelper", "qh");
		c.put("DamageSource", "ro");
		c.put("Minecraft", "bao");
		c.put("ResourceLocation", "bqx");
		c.put("EntityClientPlayerMP", "bjk");
		c.put("TextureManager", "bqf");
		c.put("Tessellator", "bmh");
		c.put("WorldClient", "bjf");
		c.put("GuiIngame", "bbv");
		c.put("IBlockAccess", "ahl");
	}
	
	@Override
	public byte[] transform(String className, String arg1, byte[] data)
	{
		byte[] newData = data;

		if(className.equals(c.get("Block")))
		{
			FMLLog.fine("[GasesCore]Patching obfuscated class: %s(Block)...", className);
			newData = patchClassBlock(data, true);
		}
		else if(className.equals("net.minecraft.block.Block"))
		{
			FMLLog.fine("[GasesCore]Patching class: %s(Block)...", className);
			newData = patchClassBlock(data, false);
		}
		else if(className.equals(c.get("BlockFire")))
		{
			FMLLog.fine("[GasesCore]Patching obfuscated class: %s(BlockFire)...", className);
			newData = patchClassBlockFire(data, true);
		}
		else if(className.equals("net.minecraft.block.BlockFire"))
		{
			FMLLog.fine("[GasesCore]Patching class: %s(BlockFire)...", className);
			newData = patchClassBlockFire(data, false);
		}
		else if(className.equals(c.get("BlockLiquid")))
		{
			FMLLog.fine("[GasesCore]Patching obfuscated class: %s(BlockLiquid)...", className);
			newData = patchClassBlockLiquid(data, true);
		}
		else if(className.equals("net.minecraft.block.BlockLiquid"))
		{
			FMLLog.fine("[GasesCore]Patching class: %s(BlockLiquid)...", className);
			newData = patchClassBlockLiquid(data, false);
		}
		else if(className.equals(c.get("BlockDynamicLiquid")))
		{
			FMLLog.fine("[GasesCore]Patching obfuscated class: %s(BlockDynamicLiquid)...", className);
			newData = patchClassBlockDynamicLiquid(data, true);
		}
		else if(className.equals("net.minecraft.block.BlockDynamicLiquid"))
		{
			FMLLog.fine("[GasesCore]Patching class: %s(BlockDynamicLiquid)...", className);
			newData = patchClassBlockDynamicLiquid(data, false);
		}

		if(newData != data)
		{
			FMLLog.fine("[GasesCore]Patch OK!");
		}
		
		return newData;
	}
	
	public byte[] dummyTransformFunc(byte[] data, boolean obfuscated)
	{
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(data);
		classReader.accept(classNode, 0);
		
		for(int i = 0; i < classNode.methods.size(); i++)
		{
		    MethodNode method = classNode.methods.get(i);
		    if(method.name.equals(obfuscated ? "h" : "updateLightmap") && method.desc.equals("(F)V"))
		    {
		    	InsnList newInstructions = new InsnList();
		    	for(int j = 0; j < method.instructions.size(); j++)
		    	{
		    		AbstractInsnNode instruction = method.instructions.get(j);
		    		newInstructions.add(instruction);
		    		if(instruction.getOpcode() == GETFIELD)
					{
		    			FieldInsnNode fieldInstruction = (FieldInsnNode)instruction;
		    			if(fieldInstruction.name.equals(obfuscated ? "ak" : "gammaSetting") & fieldInstruction.owner.equals(obfuscated ? "aui" : "net/minecraft/client/settings/GameSettings"))
		    			{
		    				newInstructions.add(new InsnNode(POP));
		    				newInstructions.add(new LdcInsnNode(-1.0F));
		    			}
					}
		    	}
		    	method.instructions = newInstructions;
		    }
		}
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		return writer.toByteArray();
	}
	
	public byte[] patchClassBlockDynamicLiquid(byte[] data, boolean obfuscated)
	{
		String classWorld = obfuscated ? c.get("World") : "net/minecraft/world/World";
		String classBlock = obfuscated ? c.get("Block") : "net/minecraft/block/Block";
		String classBlocks = obfuscated ? c.get("Blocks") : "net/minecraft/init/Blocks";
		String classMaterial = obfuscated ? c.get("Material") : "net/minecraft/block/material/Material";
		String classMaterialLiquid = obfuscated ? c.get("Material") : "net/minecraft/block/material/MaterialLiquid";
		
		String methodUpdateTick = obfuscated ? "a" : "updateTick";
		String methodGetBlock = obfuscated ? "a" : "getBlock";
		String methodSetBlock = obfuscated ? "b" : "setBlock";
		String methodGetMaterial = obfuscated ? "o" : "getMaterial";

		String fieldStone = obfuscated ? "b" : "stone";
		String fieldAir = obfuscated ? "a" : "air";

		String descriptor = "(L" + classWorld + ";IIILjava/util/Random;)V";
		
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(data);
		classReader.accept(classNode, 0);
		
		for(int i = 0; i < classNode.methods.size(); i++)
		{
			MethodNode method = classNode.methods.get(i);
			if(method.name.equals(methodUpdateTick) & method.desc.equals(descriptor))
			{
				InsnList newInstructions = new InsnList();
				for(int j = 0; j < method.instructions.size(); j++)
				{
					AbstractInsnNode instruction = method.instructions.get(j);
					newInstructions.add(instruction);
					
					if(instruction.getOpcode() == INVOKEVIRTUAL)
					{
						MethodInsnNode methodInsnNode = (MethodInsnNode)instruction;
						if(methodInsnNode.owner.equals(classWorld) && methodInsnNode.name.equals(methodSetBlock) && methodInsnNode.desc.equals("(IIIL" + classBlock + ";)Z"))
						{
							AbstractInsnNode dependentInstruction = method.instructions.get(j - 1);
							if(dependentInstruction.getOpcode() == GETSTATIC)
							{
								FieldInsnNode fieldInsnNode = (FieldInsnNode)dependentInstruction;
								if(fieldInsnNode.owner.equals(classBlocks) && fieldInsnNode.name.equals(fieldStone) && fieldInsnNode.desc.equals("L" + classBlock + ";"))
								{
									LabelNode exitLabel = new LabelNode();
									LabelNode entryLabel = new LabelNode();

									newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "configurations", "Lglenn/gases/common/GasesMainConfigurations;"));
									newInstructions.add(new FieldInsnNode(GETFIELD, "glenn/gases/common/GasesMainConfigurations", "gases_steamOnReaction", "I"));
									newInstructions.add(new JumpInsnNode(IFEQ, exitLabel));
									newInstructions.add(new VarInsnNode(ALOAD, 1));
									newInstructions.add(new VarInsnNode(ILOAD, 2));
									newInstructions.add(new VarInsnNode(ILOAD, 3));
									newInstructions.add(new VarInsnNode(ILOAD, 4));
									newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classWorld, methodGetBlock, "(III)L" + classBlock + ";", false));
									newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classBlock, methodGetMaterial, "()L" + classMaterial + ";", false));
									newInstructions.add(new FieldInsnNode(GETSTATIC, classMaterial, fieldAir, "L" + classMaterial + ";"));
									newInstructions.add(new JumpInsnNode(IF_ACMPEQ, entryLabel));
									newInstructions.add(new VarInsnNode(ALOAD, 1));
									newInstructions.add(new VarInsnNode(ILOAD, 2));
									newInstructions.add(new VarInsnNode(ILOAD, 3));
									newInstructions.add(new VarInsnNode(ILOAD, 4));
									newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classWorld, methodGetBlock, "(III)L" + classBlock + ";", false));
									newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classBlock, methodGetMaterial, "()L" + classMaterial + ";", false));
									newInstructions.add(new TypeInsnNode(INSTANCEOF, classMaterialLiquid));
									newInstructions.add(new JumpInsnNode(IFEQ, exitLabel));
									newInstructions.add(entryLabel);
									newInstructions.add(new InsnNode(POP));
									newInstructions.add(new VarInsnNode(ALOAD, 1));
									newInstructions.add(new VarInsnNode(ILOAD, 2));
									newInstructions.add(new VarInsnNode(ILOAD, 3));
									newInstructions.add(new VarInsnNode(ILOAD, 4));
									newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "gasTypeSteam", "Lglenn/gasesframework/api/gastype/GasType;"));
									newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "configurations", "Lglenn/gases/common/GasesMainConfigurations;"));
									newInstructions.add(new FieldInsnNode(GETFIELD, "glenn/gases/common/GasesMainConfigurations", "gases_steamOnReaction", "I"));
									newInstructions.add(new MethodInsnNode(INVOKESTATIC, "glenn/gasesframework/api/GasesFrameworkAPI", "placeGas", "(L" + classWorld + ";IIILglenn/gasesframework/api/gastype/GasType;I)V", false));
									newInstructions.add(new InsnNode(ICONST_1));
									newInstructions.add(exitLabel);
								}
							}
						}
					}
				}
				
				method.instructions = newInstructions;
			}
		}
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}
	
	public byte[] patchClassBlockLiquid(byte[] data, boolean obfuscated)
	{
		String classWorld = obfuscated ? c.get("World") : "net/minecraft/world/World";
		String classBlock = obfuscated ? c.get("Block") : "net/minecraft/block/Block";
		String classMaterial = obfuscated ? c.get("Material") : "net/minecraft/block/material/Material";
		String classMaterialLiquid = obfuscated ? c.get("Material") : "net/minecraft/block/material/MaterialLiquid";
		
		//checkForHarden
		String methodFunc_149805_n = obfuscated ? "n" : "func_149805_n";
		String methodGetBlock = obfuscated ? "a" : "getBlock";
		String methodSetBlock = obfuscated ? "b" : "setBlock";
		String methodGetMaterial = obfuscated ? "o" : "getMaterial";
		
		String fieldAir = obfuscated ? "a" : "air";
		
		String descriptor = "(L" + classWorld + ";III)V";
		
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(data);
		classReader.accept(classNode, 0);
		
		for(int i = 0; i < classNode.methods.size(); i++)
		{
			MethodNode method = classNode.methods.get(i);
			if(method.name.equals(methodFunc_149805_n) & method.desc.equals(descriptor))
			{
				InsnList newInstructions = new InsnList();
				for(int j = 0; j < method.instructions.size(); j++)
				{
					AbstractInsnNode instruction = method.instructions.get(j);
					newInstructions.add(instruction);
					
					if(instruction.getOpcode() == POP)
					{
						AbstractInsnNode dependentInstruction = method.instructions.get(j - 1);
						if(dependentInstruction.getOpcode() == INVOKEVIRTUAL)
						{
							MethodInsnNode methodInsnNode = (MethodInsnNode)dependentInstruction;
							if(methodInsnNode.owner.equals(classWorld) && methodInsnNode.name.equals(methodSetBlock) && methodInsnNode.desc.equals("(IIIL" + classBlock + ";)Z"))
							{
								LabelNode exitLabel = new LabelNode();
								LabelNode entryLabel = new LabelNode();
								
								newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "configurations", "Lglenn/gases/common/GasesMainConfigurations;"));
								newInstructions.add(new FieldInsnNode(GETFIELD, "glenn/gases/common/GasesMainConfigurations", "gases_steamOnReaction", "I"));
								newInstructions.add(new JumpInsnNode(IFEQ, exitLabel));
								newInstructions.add(new VarInsnNode(ALOAD, 1));
								newInstructions.add(new VarInsnNode(ILOAD, 2));
								newInstructions.add(new VarInsnNode(ILOAD, 3));
								newInstructions.add(new InsnNode(ICONST_1));
								newInstructions.add(new InsnNode(IADD));
								newInstructions.add(new VarInsnNode(ILOAD, 4));
								newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classWorld, methodGetBlock, "(III)L" + classBlock + ";", false));
								newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classBlock, methodGetMaterial, "()L" + classMaterial + ";", false));
								newInstructions.add(new FieldInsnNode(GETSTATIC, classMaterial, fieldAir, "L" + classMaterial + ";"));
								newInstructions.add(new JumpInsnNode(IF_ACMPEQ, entryLabel));
								newInstructions.add(new VarInsnNode(ALOAD, 1));
								newInstructions.add(new VarInsnNode(ILOAD, 2));
								newInstructions.add(new VarInsnNode(ILOAD, 3));
								newInstructions.add(new InsnNode(ICONST_1));
								newInstructions.add(new InsnNode(IADD));
								newInstructions.add(new VarInsnNode(ILOAD, 4));
								newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classWorld, methodGetBlock, "(III)L" + classBlock + ";", false));
								newInstructions.add(new MethodInsnNode(INVOKEVIRTUAL, classBlock, methodGetMaterial, "()L" + classMaterial + ";", false));
								newInstructions.add(new TypeInsnNode(INSTANCEOF, classMaterialLiquid));
								newInstructions.add(new JumpInsnNode(IFEQ, exitLabel));
								newInstructions.add(entryLabel);
								newInstructions.add(new VarInsnNode(ALOAD, 1));
								newInstructions.add(new VarInsnNode(ILOAD, 2));
								newInstructions.add(new VarInsnNode(ILOAD, 3));
								newInstructions.add(new InsnNode(ICONST_1));
								newInstructions.add(new InsnNode(IADD));
								newInstructions.add(new VarInsnNode(ILOAD, 4));
								newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "gasTypeSteam", "Lglenn/gasesframework/api/gastype/GasType;"));
								newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gases/Gases", "configurations", "Lglenn/gases/common/GasesMainConfigurations;"));
								newInstructions.add(new FieldInsnNode(GETFIELD, "glenn/gases/common/GasesMainConfigurations", "gases_steamOnReaction", "I"));
								newInstructions.add(new MethodInsnNode(INVOKESTATIC, "glenn/gasesframework/api/GasesFrameworkAPI", "placeGas", "(L" + classWorld + ";IIILglenn/gasesframework/api/gastype/GasType;I)V", false));
								newInstructions.add(exitLabel);
							}
						}
					}
				}
				
				method.instructions = newInstructions;
			}
		}
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}

	public byte[] patchClassBlockFire(byte[] data, boolean obfuscated)
	{
		String classWorld = obfuscated ? c.get("World") : "net/minecraft/world/World";

		String methodTryCatchFire = "tryCatchFire";
		String methodSetBlockToAir = obfuscated ? "f" : "setBlockToAir";
		
		String descriptor = "(L" + classWorld + ";IIIILjava/util/Random;ILnet/minecraftforge/common/util/ForgeDirection;)V";
		
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(data);
		classReader.accept(classNode, 0);
		
		LabelNode l2 = new LabelNode();
		
		for(int i = 0; i < classNode.methods.size(); i++)
		{
			MethodNode method = (MethodNode)classNode.methods.get(i);
			if(method.name.equals(methodTryCatchFire) & method.desc.equals(descriptor))
			{
				InsnList newInstructions = new InsnList();
				for(int j = 0; j < method.instructions.size(); j++)
				{
					AbstractInsnNode instruction = method.instructions.get(j);
					AbstractInsnNode instruction2 = (j + 4 < method.instructions.size()) ? method.instructions.get(j + 4) : null;
					if(instruction2 != null && instruction2.getOpcode() == INVOKEVIRTUAL)
					{
						MethodInsnNode invokeInstruction = (MethodInsnNode)instruction2;
						if(invokeInstruction.name.equals(methodSetBlockToAir))
						{
							newInstructions.add(new VarInsnNode(ALOAD, 1));
							newInstructions.add(new VarInsnNode(ILOAD, 2));
							newInstructions.add(new VarInsnNode(ILOAD, 3));
							newInstructions.add(new VarInsnNode(ILOAD, 4));
							newInstructions.add(new FieldInsnNode(GETSTATIC, "glenn/gasesframework/api/GasesFrameworkAPI", "gasTypeSmoke", "Lglenn/gasesframework/api/gastype/GasType;"));
							newInstructions.add(new MethodInsnNode(INVOKESTATIC, "glenn/gasesframework/api/GasesFrameworkAPI", "getFireSmokeAmount", "()I", false));
							newInstructions.add(new MethodInsnNode(INVOKESTATIC, "glenn/gasesframework/api/GasesFrameworkAPI", "placeGas", "(L" + classWorld + ";IIILglenn/gasesframework/api/gastype/GasType;I)V", false));
							newInstructions.add(new InsnNode(ICONST_1));
							newInstructions.add(new JumpInsnNode(GOTO, l2));
						}
					}
					
					newInstructions.add(instruction);
					
					if(instruction.getOpcode() == INVOKEVIRTUAL)
					{
						MethodInsnNode invokeInstruction = (MethodInsnNode)instruction;
						if(invokeInstruction.name.equals(methodSetBlockToAir))
						{
							newInstructions.add(l2);
						}
					}
				}
				method.instructions = newInstructions;
			}
		}

		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		return writer.toByteArray();
	}

	public byte[] patchClassBlock(byte[] data, boolean obfuscated)
	{
		String methodRegisterBlocks = obfuscated ? "p" : "registerBlocks";
		
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(data);
		classReader.accept(classNode, 0);
		
		boolean bedrockFound = false;
		boolean coal_oreFound = false;

		for(int i = 0; i < classNode.methods.size(); i++)
		{
			MethodNode method = classNode.methods.get(i);
			if(method.name.equals(methodRegisterBlocks) && method.desc.equals("()V"))
			{
				for(int j = 0; j < method.instructions.size(); j++)
				{
					AbstractInsnNode instruction = method.instructions.get(j);
					if(instruction.getOpcode() == LDC)
					{
						LdcInsnNode ldcInstruction = (LdcInsnNode)instruction;
						if(ldcInstruction.cst.equals("bedrock"))
						{
							if(!bedrockFound)
							{
								((TypeInsnNode)method.instructions.get(j + 1)).desc = "glenn/gases/common/block/BlockBedrock";
								((MethodInsnNode)method.instructions.get(j + 4)).owner = "glenn/gases/common/block/BlockBedrock";
							}
							bedrockFound = true;
						}
						else if(ldcInstruction.cst.equals("coal_ore"))
						{
							if(!coal_oreFound)
							{
								((TypeInsnNode)method.instructions.get(j + 1)).desc = "glenn/gases/common/block/BlockCoalOre";
								((MethodInsnNode)method.instructions.get(j + 3)).owner = "glenn/gases/common/block/BlockCoalOre";
							}
							coal_oreFound = true;
						}
					}
				}
			}
		}

		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		return writer.toByteArray();
	}
}
