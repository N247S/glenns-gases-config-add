package glenn.gases.common.worldgen;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneratorGases implements IWorldGenerator
{
	private WorldGenerator gasPipeGen;

    public WorldGeneratorGases()
    {
    	gasPipeGen = new WorldGenPipes(GasesFrameworkAPI.gasTypeAir.pipeBlock);
    }

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		int chunk_X = chunkX * 16;
		int chunk_Z = chunkZ * 16;
		
		String s = chunkGenerator.makeString();
		
		if(world.provider.getDimensionName().toLowerCase().equals("overworld"))
		{
			for(int i = 0; i < Gases.configurations.world_generatePipes; i++)
			{
				gasPipeGen.generate(world, random, chunk_X + random.nextInt(16), 16 + random.nextInt(32), chunk_Z + random.nextInt(16));
			}
		}
	}
}