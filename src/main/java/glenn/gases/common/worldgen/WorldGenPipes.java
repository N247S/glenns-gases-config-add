package glenn.gases.common.worldgen;

import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.moddingutils.IVec;

import java.util.HashSet;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.util.ForgeDirection;

public class WorldGenPipes extends WorldGenerator
{
	protected Block pipeBlock;
	protected HashSet<IVec> pipePlacements;
	
	public WorldGenPipes(Block pipeBlock)
	{
		this.pipeBlock = pipeBlock;
	}
	
	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		if(world.getBlock(x, y, z) != Blocks.air) return false;
		
		pipePlacements = new HashSet<IVec>();
		
		branch(world, random, x, y, z);
		
		if(pipePlacements.size() > 6)
		{
			for(IVec pipe : pipePlacements)
			{
				if(random.nextInt(12) != 0) world.setBlock(pipe.x, pipe.y, pipe.z, pipeBlock);
			}
		}
		
		return true;
	}
	
	protected Block getBlock(World world, int x, int y, int z)
	{
		if(pipePlacements.contains(new IVec(x, y, z)))
		{
			return pipeBlock;
		}
		else
		{
			return world.getBlock(x, y, z);
		}
	}
	
	protected void branch(World world, Random random, int x, int y, int z)
	{
		boolean[] sides = new boolean[6];
		int generationChance = 1;
		
		for(int i = 0; i < 6; i++)
		{
			int xDirection = i == 4 ? 1 : (i == 5 ? -1 : 0);
    		int yDirection = i == 0 ? 1 : (i == 1 ? -1 : 0);
    		int zDirection = i == 2 ? 1 : (i == 3 ? -1 : 0);
    		
    		Block block = getBlock(world, x + xDirection, y + yDirection, z + zDirection);
    		if(block.isOpaqueCube())
    		{
    			sides[i] = true;
    		}
    		else if(block == Blocks.air)
    		{
    			generationChance++;
    		}
		}
		
		generationChance = generationChance / 2 + 1;
		
		boolean[] possibleDirections = new boolean[]{
			random.nextInt(generationChance) == 0 && getBlock(world, x, y + 1, z) == Blocks.air & (sides[2] | sides[3] | sides[4] | sides[5]),
			random.nextInt(generationChance) == 0 && getBlock(world, x, y - 1, z) == Blocks.air & (sides[2] | sides[3] | sides[4] | sides[5]),
			random.nextInt(generationChance) == 0 && getBlock(world, x, y, z + 1) == Blocks.air & (sides[0] | sides[1] | sides[4] | sides[5]),
			random.nextInt(generationChance) == 0 && getBlock(world, x, y, z - 1) == Blocks.air & (sides[0] | sides[1] | sides[4] | sides[5]),
			random.nextInt(generationChance) == 0 && getBlock(world, x + 1, y, z) == Blocks.air & (sides[0] | sides[1] | sides[2] | sides[3]),
			random.nextInt(generationChance) == 0 && getBlock(world, x - 1, y, z) == Blocks.air & (sides[0] | sides[1] | sides[2] | sides[3])
		};
		
		int nPossibleDirections = 0;
		
		for(int i = 0; i < 6; i++)
		{
			if(possibleDirections[i])
			{
				nPossibleDirections++;
			}
		}
		
		if(nPossibleDirections > 0)
		{
			pipePlacements.add(new IVec(x, y, z));
		}
		
		while(nPossibleDirections > 0)
		{
			int direction = random.nextInt(6);
			if(possibleDirections[direction])
			{
				possibleDirections[direction] = false;
				nPossibleDirections--;
				
				generatePipe(world, random, x, y, z, ForgeDirection.VALID_DIRECTIONS[direction]);
			}
		}
	}
	
	protected void generatePipe(World world, Random random, int x, int y, int z, ForgeDirection startDirection)
	{
		int support = 2;
		
		for(int i = 0; i < 10; i++)
		{
			x += startDirection.offsetX;
			y += startDirection.offsetY;
			z += startDirection.offsetZ;
			
			if(support <= 0)
			{
				break;
			}
			
			if(world.isAirBlock(x, y, z))
			{
				boolean flag = false;
				boolean flag2 = false;
				
				for(ForgeDirection direction : ForgeDirection.VALID_DIRECTIONS)
				{
					if(direction.getOpposite() == startDirection) continue;
		    		
					int x1 = x + direction.offsetX;
					int y1 = y + direction.offsetY;
					int z1 = z + direction.offsetZ;
					
		    		Block block = getBlock(world, x1, y1, z1);
		    		if(block.isOpaqueCube())
		    		{
		    			flag = true;
		    		}
		    		else if(GasesFrameworkAPI.getGasPipeType(world, x1, y1, z1) != null)
		    		{
		    			flag2 = true;
		    		}
				}
				
				if(flag2) support = -1;
				else if(flag) support = 2;
				else support--;
	    		
				int x1 = x + startDirection.offsetX;
				int y1 = y + startDirection.offsetY;
				int z1 = z + startDirection.offsetZ;
				
				Block block = getBlock(world, x1, y1, z1);
				if((support == 2 && random.nextInt(10) == 0) || random.nextInt(2) == 0 && block != Blocks.air && GasesFrameworkAPI.getGasPipeType(world, x1, y1, z1) == null)
				{
					branch(world, random, x, y, z);
					break;
				}
				else
				{
					pipePlacements.add(new IVec(x, y, z));
				}
			}
			else
			{
				break;
			}
		}
	}
}
