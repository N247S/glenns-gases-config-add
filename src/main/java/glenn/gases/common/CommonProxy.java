package glenn.gases.common;

import net.minecraftforge.common.MinecraftForge;

/** Proxy parent object. */
public class CommonProxy
{
	/** Registers custom renderers. */
	public void registerRenderers()
	{
		
	}
	
	/** Registers event handlers. */
	public void registerEventHandlers()
	{
		MinecraftForge.EVENT_BUS.register(new ForgeCommonEvents());
	}
}