package glenn.gases.common.reaction;

import glenn.gasesframework.api.reaction.ReactionBase;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class ReactionExtinguish extends ReactionBase
{
	public ReactionExtinguish(int priority, Block gasBlock, Block blockToExtinguish)
	{
		super(priority, gasBlock, blockToExtinguish);
	}

	@Override
	protected boolean reactBoth(World world, Random random, int block1x, int block1y, int block1z, int block2x, int block2y, int block2z)
	{
		return false;
	}

	@Override
	protected boolean reactBlock1(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block block = world.getBlock(blockX, blockY, blockZ);
		if(block == reactionBlock2)
		{
			block.dropBlockAsItem(world, blockX, blockY, blockZ, world.getBlockMetadata(blockX, blockY, blockZ), 0);
			world.setBlockToAir(blockX, blockY, blockZ);
			return true;
		}
		return false;
	}

	@Override
	protected boolean reactBlock2(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block block = world.getBlock(blockX, blockY, blockZ);
		if(block == reactionBlock2)
		{
			block.dropBlockAsItem(world, blockX, blockY, blockZ, world.getBlockMetadata(blockX, blockY, blockZ), 0);
			world.setBlockToAir(blockX, blockY, blockZ);
			return true;
		}
		return false;
	}
}