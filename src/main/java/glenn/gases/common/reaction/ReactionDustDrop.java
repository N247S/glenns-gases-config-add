package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.reaction.ReactionSpecialEvent;
import glenn.moddingutils.DVec;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ReactionDustDrop extends ReactionSpecialEvent
{
	public ReactionDustDrop(Block block1, Block block2)
	{
		super(20, block1, block2, true, true);
	}

	@Override
	public int getDelay(World world, int block1X, int block1Y, int block1Z, int block2X, int block2Y, int block2Z)
	{
		return 0;
	}

	@Override
	protected boolean reactBoth(World world, Random random, int block1x, int block1y, int block1z, int block2x, int block2y, int block2z)
	{
    	if(!world.isRemote)
		{
			double x = (block1x + block2x + 1) * 0.5D;
			double y = (block1y + block2y + 1) * 0.5D;
			double z = (block1z + block2z + 1) * 0.5D;
			
			if(random.nextInt(8) == 0)
			{
				ItemStack item = random.nextBoolean() ? new ItemStack(Gases.turquoiseDust) : new ItemStack(Gases.blueDust);
				DVec vec = DVec.randomNormalizedVec(random).scale(0.1D);
				EntityItem itemEntity = new EntityItem(world, x, y, z, item);
				itemEntity.addVelocity(vec.x, vec.y, vec.z);
				world.spawnEntityInWorld(itemEntity);
			}
		}
		
		return true;
	}
}
