package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.reaction.Reaction;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class ReactionCorrosion extends Reaction
{
	protected Block gasBlock;
	
	public ReactionCorrosion(Block gasBlock)
	{
		super(30);
		this.gasBlock = gasBlock;
	}

	@Override
	public boolean isErroneous()
	{
		return false;
	}

	@Override
	public boolean is(World world, Block block1, int block1X, int block1Y, int block1Z, Block block2, int block2X, int block2Y, int block2Z)
	{
		if(block1 == gasBlock & block2 != Blocks.air)
		{
			float hardness = block2.getBlockHardness(world, block2X, block2Y, block2Z);
			return !(block2 instanceof Block) & hardness != -1.0F & hardness < Gases.configurations.gases_maxHardnessForCorrosion;
		}
		else if(block1 != Blocks.air)
		{
			float hardness = block1.getBlockHardness(world, block1X, block1Y, block1Z);
			return !(block1 instanceof Block) & hardness != -1.0F & hardness < Gases.configurations.gases_maxHardnessForCorrosion;
		}
		
		return false;
	}

	@Override
	protected boolean reactBoth(World world, Random random, int block1x, int block1y, int block1z, int block2x, int block2y, int block2z)
	{
		return false;
	}

	@Override
	protected boolean reactBlock1(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block block = world.getBlock(blockX, blockY, blockZ);
		if(block != gasBlock & random.nextInt(10) == 0)
		{
			if(random.nextInt(10) == 0)
			{
				block.dropBlockAsItem(world, blockX, blockY, blockZ, world.getBlockMetadata(blockX, blockY, blockZ), 0);
			}
			world.setBlockToAir(blockX, blockY, blockZ);
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	protected boolean reactBlock2(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block block = world.getBlock(blockX, blockY, blockZ);
		if(block != gasBlock & random.nextInt(10) == 0)
		{
			if(random.nextInt(10) == 0)
			{
				block.dropBlockAsItem(world, blockX, blockY, blockZ, world.getBlockMetadata(blockX, blockY, blockZ), 0);
			}
			world.setBlockToAir(blockX, blockY, blockZ);
			return true;
		}
		else
		{
			return false;
		}
	}
}