package glenn.gases.common.reaction;

import glenn.gases.common.gastype.GasTypeChlorine;
import glenn.gasesframework.api.reaction.Reaction;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class ReactionRust extends Reaction
{
	public Block gasBlock;
	
	public ReactionRust(Block gasBlock)
	{
		super(20);
		this.gasBlock = gasBlock;
	}
	
	@Override
	public int getDelay(World world, int block1X, int block1Y, int block1Z, int block2X, int block2Y, int block2Z)
	{
		return 0;
	}

	@Override
	public boolean isErroneous()
	{
		return false;
	}

	@Override
	public boolean is(World world, Block block1, int block1X, int block1Y, int block1Z, Block block2, int block2X, int block2Y, int block2Z)
	{
		return (GasTypeChlorine.blocksToRust.get(block1) != null & block2 == gasBlock) | (GasTypeChlorine.blocksToRust.get(block2) != null & block1 == gasBlock);
	}

	@Override
	protected boolean reactBoth(World world, Random random, int block1x, int block1y, int block1z, int block2x, int block2y, int block2z)
	{
		return false;
	}

	@Override
	protected boolean reactBlock1(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block replacementBlock = GasTypeChlorine.blocksToRust.get(world.getBlock(blockX, blockY, blockZ));
		if(replacementBlock != null)
		{
			world.setBlock(blockX, blockY, blockZ, replacementBlock);
			return true;
		}
		return false;
	}

	@Override
	protected boolean reactBlock2(World world, Random random, int blockX, int blockY, int blockZ)
	{
		Block replacementBlock = GasTypeChlorine.blocksToRust.get(world.getBlock(blockX, blockY, blockZ));
		if(replacementBlock != null)
		{
			world.setBlock(blockX, blockY, blockZ, replacementBlock);
			return true;
		}
		return false;
	}
}