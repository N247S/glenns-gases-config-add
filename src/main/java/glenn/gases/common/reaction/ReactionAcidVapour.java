package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.gasesframework.api.reaction.ReactionBase;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class ReactionAcidVapour extends ReactionBase
{
	public ReactionAcidVapour(int priority, Block block1, Block block2)
	{
		super(priority, block1, block2);
	}
	
	@Override
	protected boolean reactBoth(World world, Random random, int block1x, int block1y, int block1z, int block2x, int block2y, int block2z)
	{
		Block block1 = world.getBlock(block1x, block1y, block1z);
		Block block2 = world.getBlock(block2x, block2y, block2z);
		
		if(block1 == reactionBlock1 | block1 == reactionBlock2)
		{
			int block1Volume = GasesFrameworkAPI.getGasVolume(world, block1x, block1y, block1z);
			GasesFrameworkAPI.placeGas(world, block1x, block1y, block1z, Gases.gasTypeAcidVapour, block1Volume);
		}
		
		if(block2 == reactionBlock1 | block2 == reactionBlock2)
		{
			int block2Volume = GasesFrameworkAPI.getGasVolume(world, block2x, block2y, block2z);
			GasesFrameworkAPI.placeGas(world, block2x, block2y, block2z, Gases.gasTypeAcidVapour, block2Volume);
		}
		
		return true;
	}
	
	@Override
	protected boolean reactBlock1(World world, Random random, int blockX, int blockY, int blockZ)
	{
		return false;
	}
	
	@Override
	protected boolean reactBlock2(World world, Random random, int blockX, int blockY, int blockZ)
	{
		return false;
	}
}