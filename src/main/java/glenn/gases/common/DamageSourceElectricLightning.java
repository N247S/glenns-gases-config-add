package glenn.gases.common;

import net.minecraft.util.DamageSource;

/** Damage source for electric gas lightning. */
public class DamageSourceElectricLightning extends DamageSource
{
	public DamageSourceElectricLightning(String localizeString)
	{
		super(localizeString);
		this.setDamageBypassesArmor();
	}
}