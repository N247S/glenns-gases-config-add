package glenn.gases.common;

import java.util.List;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

/** Server event handler. Registered in CommonProxy. */
public class ForgeCommonEvents
{
	/** Handles creation of dust. */
	@SubscribeEvent
	public void onBlockBreak(BreakEvent event)
	{
		if(!event.world.isRemote && event.block.getMaterial() == Material.rock && event.block != Blocks.coal_ore)
		{
			int side = event.world.rand.nextInt(6);
			int xDirection = event.x + (side == 4 ? 1 : (side == 5 ? -1 : 0));
	    	int yDirection = event.y + (side == 0 ? 1 : (side == 1 ? -1 : 0));
	    	int zDirection = event.z + (side == 2 ? 1 : (side == 3 ? -1 : 0));
	    	
	    	if(event.world.isAirBlock(xDirection, yDirection, zDirection))
	    	{
	    		GasesFrameworkAPI.placeGas(event.world, xDirection, yDirection, zDirection, Gases.gasTypeDust, Gases.configurations.gases_dustOnMine);
	    	}
		}
	}
	
	@SubscribeEvent
	public void onBlockDrop(HarvestDropsEvent event)
	{
		if(isCoalBlock(event.block) && !event.isSilkTouching)
		{
			GasesFrameworkAPI.placeGas(event.world, event.x, event.y, event.z, Gases.gasTypeCoalDust, Gases.configurations.gases_coalDustOnMine);
		}
	}
	
	public boolean isCoalBlock(Block block)
	{
		ItemStack blockType = new ItemStack(block, 1);
		List<ItemStack> coalBlockList = Gases.CoalBlockTypeList;
		if(Gases.CoalBlockTypeList != null)
		{
			for(int i = 0; i < coalBlockList.size(); i++)
			{
				if(coalBlockList.get(i).isItemEqual(blockType))
					return true;
			}
		}
		return false;
	}
}
