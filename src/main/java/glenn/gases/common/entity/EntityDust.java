package glenn.gases.common.entity;

import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.gasesframework.api.block.MaterialGas;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public abstract class EntityDust extends EntityFX
{
	private static final ResourceLocation texture = new ResourceLocation("gases:textures/effects/dust.png");
	
	protected EntityLivingBase thrower;
	
    public EntityDust(World world)
    {
    	super(world);
    	
    }
	
	public EntityDust(World world, double x, double y, double z)
	{
		super(world, x, y, z);
	}
    
	public EntityDust(World world, EntityLivingBase thrower)
	{
		this(world);
		this.thrower = thrower;
		setPosition(thrower.posX - (double)(MathHelper.cos(thrower.rotationYaw / 180.0F * (float)Math.PI) * 0.16F),
				thrower.posY + thrower.getEyeHeight(),
				thrower.posZ - (double)(MathHelper.sin(thrower.rotationYaw / 180.0F * (float)Math.PI) * 0.16F));
        
        double rx = rand.nextDouble() - 0.5D;
        double ry = rand.nextDouble() - 0.5D;
        double rz = rand.nextDouble() - 0.5D;
        double n = Math.sqrt(rx * rx + ry * ry + rz * rz) * 2.0D / rand.nextDouble();
        
		double d = 0.8D;
		this.motionX = (-MathHelper.sin(thrower.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(thrower.rotationPitch / 180.0F * (float)Math.PI) * d) + thrower.motionX + rx / n;
        this.motionZ = (MathHelper.cos(thrower.rotationYaw / 180.0F * (float)Math.PI) * MathHelper.cos(thrower.rotationPitch / 180.0F * (float)Math.PI) * d) + thrower.motionZ + rz / n;
        this.motionY = (-MathHelper.sin((thrower.rotationPitch) / 180.0F * (float)Math.PI) * d) + thrower.motionY + ry / n;
        
        maxLifetime = 50 + rand.nextInt(50);
	}
	
	public abstract GasType getReactiveType();
	
	public abstract ItemStack getResult();
	
	public void onInGas()
	{
		int x = MathHelper.floor_double(posX);
		int y = MathHelper.floor_double(posY);
		int z = MathHelper.floor_double(posZ);
		
		if(GasesFrameworkAPI.getGasType(worldObj, x, y, z) == getReactiveType())
		{
			if(rand.nextInt(12) < 16 - worldObj.getBlockMetadata(x, y, z))
			{
				worldObj.spawnEntityInWorld(new EntityItem(worldObj, x + 0.5D, y + 0.5D, z + 0.5D, getResult()));
			}
			
			worldObj.setBlock(x, y, z, Blocks.air);
		}
	}
	
	@Override
	public double getScale(float partialTick)
	{
		return 0.1D + (double)(lifetime + partialTick) / 500.D;
	}
	
	@Override
	public void onUpdate()
    {
		
		motionX *= 0.9D;
        motionY = motionY * 0.9D + 0.001D;
        motionZ *= 0.9D;
        
		super.onUpdate();
        
        if(!worldObj.isRemote && isInsideOfMaterial(MaterialGas.INSTANCE))
        {
        	onInGas();
        }
    }
	
	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}
	
	@Override
	protected boolean isServerSide()
	{
		return true;
	}
}