package glenn.gases.common.block;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import net.minecraft.block.Block;
import net.minecraft.block.BlockOre;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.world.World;

/** A modified coal ore block that emits coal dust on mine. Overrides regular coal ore in world generation. */
public class BlockCoalOre extends BlockOre
{
    /** Creates a new, modified coal ore block that will emit coal dust on mine. */
	public BlockCoalOre()
	{
		super();
        this.setCreativeTab(CreativeTabs.tabBlock);
	}
	
	/** Drops the block items with a specified chance of dropping the specified items */
	@Override
	public void dropBlockAsItemWithChance(World world, int x, int y, int z, int par5, float par6, int par7)
	{
		super.dropBlockAsItemWithChance(world, x, y, z, par5, par6, par7);
		//this.dropXpOnBlockBreak(world, x, y, z, MathHelper.getRandomIntegerInRange(world.rand, 0, 2));
		if(Gases.configurations.gases_coalDustOnMine > 0)
		{
			GasesFrameworkAPI.placeGas(world, x, y, z, Gases.gasTypeCoalDust, Gases.configurations.gases_coalDustOnMine);
		}
	}
	
	/** Sets hardness of this block. */
	@Override
	public Block setHardness(float hardness)
	{
		return super.setHardness(hardness);
	}
}