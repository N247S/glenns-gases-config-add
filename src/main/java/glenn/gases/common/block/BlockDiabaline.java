package glenn.gases.common.block;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.gasesframework.api.gastype.GasType;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

/** Diabaline block. Crafted with 9x refined diabaline. Similar to other "ore" blocks, e.g. diamond block, iron block. */
public class BlockDiabaline extends Block
{
	protected boolean glowing;
	
	/** Creates a new diabaline block.
	 * @param glowing Sets the block to glowing if true or non-glowing if false.*/
	public BlockDiabaline(boolean glowing)
	{
		super(Material.iron);
		this.glowing = glowing;
 		
		if(glowing) setLightLevel(1.0F);
		setHardness(1.5F);
		setResistance(10.0F);
		setStepSound(Block.soundTypePiston);
		this.setTickRandomly(true);
	}
	
	/** Fires when block is placed in the world. */
	@Override
    public void onBlockAdded(World world, int x, int y, int z)
    {
		world.scheduleBlockUpdate(x, y, z, this, tickRate(world));
    }

	/** Fires when block is mined. */
	@Override
    public Item getItemDropped(int metadata, Random random, int fortune) {
        return Item.getItemFromBlock(Gases.diabalineBlock);
    }

	/** Ticks the block if it's been scheduled. Should always be scheduled. */
	@Override
    public void updateTick(World world, int x, int y, int z, Random random)
    {
    	int radius = 3;
    	boolean isSurroundedByGas = false;
    	
    	for(int i = -radius; i <= radius; i++)
    	{
    		for(int j = -radius; j <= radius; j++)
        	{
    			for(int k = -radius; k <= radius; k++)
    	    	{
    		    	GasType type = GasesFrameworkAPI.getGasType(world, x + i, y + j, z + k);
    				if(type != null && type.isIndustrial)
					{
	    	    		isSurroundedByGas = true;
	    	    		break;
					}
    	    	}
        	}
    	}
    	
    	if(isSurroundedByGas & !glowing)
    	{
    		world.setBlock(x, y, z, Gases.diabalineBlockGlowing);
    	}
    	else if(!isSurroundedByGas & glowing)
    	{
    		world.setBlock(x, y, z, Gases.diabalineBlock);
    	}
    	else
    	{
    		world.scheduleBlockUpdate(x, y, z, this, tickRate(world));
    	}
    }

    /** How many world ticks before ticking. */
	@Override
    public int tickRate(World world)
    {
        return 2;
    }
	
	/** Checks how much redstone power this block is providing. */
	@Override
    public int isProvidingWeakPower(IBlockAccess blockAccess, int p_149748_2_, int p_149748_3_, int p_149748_4_, int p_149748_5_)
    {
        return glowing ? 15 : 0;
    }
	
	/** Can this block provide power. Only wire currently seems to have this change based on its state. */
	@Override
    public boolean canProvidePower()
    {
        return true;
    }
}