package glenn.gases.common.block;

import java.util.Random;

import net.minecraft.block.BlockStone;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class BlockWarmStone extends BlockStone
{
	private IIcon[] blockIcons;
	
	public BlockWarmStone()
	{
		super();
		this.setTickRandomly(true);
	}
	
	@Override
    public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
    {
		float damage = (world.getBlockMetadata(x, y, z) - 4) / 5.0F;
		if(damage > 0.0F & !(entity instanceof EntityItem))
    	{
    		entity.attackEntityFrom(DamageSource.generic, damage);
    	}
    }
	
	@Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
    {
        float f = 0.01F;
        //return AxisAlignedBB.getAABBPool().getAABB((double)((float)x + f), (double)y, (double)((float)z + f), (double)((float)(x + 1) - f), (double)((float)(y + 1) - f), (double)((float)(z + 1) - f));
        return AxisAlignedBB.getBoundingBox((double)((float)x + f), (double)y, (double)((float)z + f), (double)((float)(x + 1) - f), (double)((float)(y + 1) - f), (double)((float)(z + 1) - f));
    }
	
	@Override
    public void registerBlockIcons(IIconRegister iconRegister)
    {
		blockIcons = new IIcon[16];
        for(int i = 0; i < blockIcons.length; i++)
        {
        	this.blockIcons[i] = iconRegister.registerIcon(getTextureName() + "_" + i);
        }
    }
	
	@Override
    public void onBlockAdded(World world, int x, int y, int z)
    {
		//world.setBlockMetadataWithNotify(x, y, z, 15, 3);
		world.scheduleBlockUpdate(x, y, z, this, 100 + world.rand.nextInt(26));
    }
	
	@Override
    public void updateTick(World world, int x, int y, int z, Random random)
    {
		int metadata = world.getBlockMetadata(x, y, z);
		if(metadata <= 0)
		{
			world.setBlock(x, y, z, Blocks.stone);
		}
		else
		{
			world.setBlockMetadataWithNotify(x, y, z, metadata - 1, 3);
		}
		world.scheduleBlockUpdate(x, y, z, this, 100 + random.nextInt(26) - metadata * 3);
    }
	
	@Override
	public IIcon getIcon(int side, int metadata)
    {
		return blockIcons[metadata];
    }
	
	/**
     * only called by clickMiddleMouseButton , and passed to inventory.setCurrentItem (along with isCreative)
     */
    @Override
    public Item getItem(World par1World, int par2, int par3, int par4)
    {
        return Item.getItemFromBlock(Blocks.stone);
    }
    
    @Override
	public Item getItemDropped(int par1, Random par2Random, int par3)
	{
	    return Item.getItemFromBlock(Blocks.stone);
	}
}
