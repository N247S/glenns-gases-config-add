package glenn.gases.common.gastype;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Random;
import java.util.Set;
import java.util.Collections;

import glenn.gases.Gases;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.item.EntityFireworkRocket;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class GasTypeFinine extends GasType
{
	private final Set<Class<?>> typeFilter = Collections.newSetFromMap(new IdentityHashMap<Class<?>, Boolean>());
	
	private boolean isValidEntityClass(Class<?> clazz)
	{
		for(Class<?> clazz1 : typeFilter)
		{
			if(clazz1.isAssignableFrom(clazz))
			{
				return true;
			}
		}
		return false;
	}
	
	public GasTypeFinine(boolean isIndustrial, int gasID, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasID, name, color, opacity, density, combustibility);
		
		typeFilter.add(EntityLivingBase.class);
		typeFilter.add(EntityItem.class);
		typeFilter.add(IProjectile.class);
		typeFilter.add(EntityBoat.class);
		typeFilter.add(EntityMinecart.class);
		typeFilter.add(EntityTNTPrimed.class);
		typeFilter.add(EntityFireworkRocket.class);
		typeFilter.add(EntityXPOrb.class);
	}
	
	/**
	 * This method is called upon gas block construction when the gas type is {@link glenn.gasesframework.api.GasesFrameworkAPI#registerGasType(GasType) registered}.
	 * @return
	 */
	@Override
	public Block tweakGasBlock(Block block)
	{
		block.setLightLevel(0.5f);
		return super.tweakGasBlock(block);
	}
	
	@Override
	public void onTouched(Entity entity)
	{
		Random rand = entity.worldObj.rand;
		if(isValidEntityClass(entity.getClass()) && rand.nextInt(4) == 0)
		{
			for(int i = 0; i < Gases.configurations.maxFinineTeleportSearches; i++)
			{
				double x = (rand.nextDouble() - rand.nextDouble()) * 4.0D;
				double y = (rand.nextDouble() - rand.nextDouble()) * 4.0D;
				double z = (rand.nextDouble() - rand.nextDouble()) * 4.0D;
				AxisAlignedBB aabb = entity.boundingBox.copy().offset(x, y, z);
				if(entity.worldObj.getCollidingBoundingBoxes(entity, aabb).isEmpty())
				{
					entity.playSound("minecraft:mob.endermen.portal", 1.0f, 1.0f);
					entity.setPosition(entity.posX + x, entity.posY + y, entity.posZ + z);
					entity.setVelocity(0.0D, 0.0D, 0.0D);
					entity.rotationPitch = rand.nextFloat() * 180.0f - 90.0f;
					entity.rotationYaw = rand.nextFloat() * 360.0f;
					entity.playSound("minecraft:mob.endermen.portal", 1.0f, 1.0f);
					break;
				}
			}
		}
	}
}