package glenn.gases.common.gastype;

import glenn.gasesframework.api.Combustibility;

import java.util.HashMap;
import java.util.HashSet;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.world.World;

/** Gas Type for Chlorine. */
public class GasTypeChlorine extends GasTypePoisonous
{
	public static final HashSet<String> materialsToDamage = new HashSet<String>();
	
	public static final HashMap<Block, Block> blocksToRust = new HashMap<Block, Block>();
	public static final HashMap<Item, Item> itemsToRust = new HashMap<Item, Item>();
	
	/** Creates this gas type. */
	public GasTypeChlorine(boolean isIndustrial, int gasIndex, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasIndex, name, color, opacity, density, combustibility);
	}
	
	/** Called when an entity touches the gas in block form.
	 * @param entity */
    @Override
	public void onTouched(Entity entity)
	{
    	super.onTouched(entity);
    	
    	if(!entity.worldObj.isRemote)
		{
    		if(entity instanceof EntityPlayer)
    		{
	    		EntityPlayer playerEntity = (EntityPlayer)entity;
				InventoryPlayer inventory = playerEntity.inventory;
				for(int i = 0; i < 4; i++)
				{
					ItemStack stack = inventory.armorItemInSlot(i);
					if(stack != null && entity.worldObj.rand.nextInt(10) == 0)
					{
						Item item = stack.getItem();
						if(item instanceof ItemArmor)
						{
							ItemArmor.ArmorMaterial armorMaterial = ((ItemArmor)item).getArmorMaterial();
							String armorName = armorMaterial.name();
							if(materialsToDamage.contains(armorName))
							{
								stack.damageItem(1, playerEntity);
								if(stack.stackSize == 0)
								{
									inventory.armorInventory[i] = null;
								}
							}
						}
					}
				}
    		}
    		else if(entity instanceof EntityItem)
    		{
    			ItemStack newItemStack = null;
    			
    			EntityItem itemEntity = (EntityItem)entity;
    			ItemStack itemStack = itemEntity.getEntityItem();
    			Item item = itemStack.getItem();
    			Item rustedItem = itemsToRust.get(item);
    			
    			if(rustedItem == null && itemStack.getItem() instanceof ItemBlock)
    			{
    				Block block = blocksToRust.get(((ItemBlock)item).field_150939_a);
    				if(block != null) rustedItem = Item.getItemFromBlock(block);
    			}
    			
    			if(rustedItem != null)
    			{
    				newItemStack = new ItemStack(rustedItem, itemStack.stackSize, itemStack.getItemDamage());
    				newItemStack.setTagCompound(itemStack.getTagCompound());
    				itemEntity.setEntityItemStack(newItemStack);
    			}
    			else
    			{
    				boolean rust = false;
    				if(item instanceof ItemArmor)
    				{
						if(materialsToDamage.contains(((ItemArmor)item).getArmorMaterial().name())) rust = true;
    				}
    				else if(item instanceof ItemTool)
    				{
    					if(materialsToDamage.contains(((ItemTool)item).getToolMaterialName())) rust = true;
    				}
    				else if(item instanceof ItemSword)
    				{
    					if(materialsToDamage.contains(((ItemSword)item).getToolMaterialName())) rust = true;
    				}
    				
    				if(rust)
    				{
    					
						if(itemStack.attemptDamageItem(1, entity.worldObj.rand))
						{
							itemEntity.setDead();
						}
    				}
    			}
    			
    			if(newItemStack != null)
    			{
    				itemEntity.setEntityItemStack(newItemStack);
    			}
    		}
		}
	}
}