package glenn.gases.common.gasworldgentype;

import glenn.gases.Gases;
import glenn.gasesframework.api.gastype.GasType;
import glenn.gasesframework.api.gasworldgentype.GasWorldGenPocket;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class GasWorldGenDiabalinePocket extends GasWorldGenPocket
{
	private static final Random random = new Random();
	
	public GasWorldGenDiabalinePocket(String name, GasType gasType, float generationFrequency, float averageVolume, float evenness, int minY, int maxY, Block replacedBlock)
	{
		super(name, gasType, generationFrequency, averageVolume, evenness, minY, maxY, replacedBlock);
	}
	
	/**
	 * Get the volume of gas placed at this location, if any. Must be a number between 0 and 16.
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param placementScore - The greater this value is, the more central this block of gas is.
	 * @return
	 */
	@Override
	public int getPlacementVolume(World world, int x, int y, int z, float placementScore)
	{
		int result = super.getPlacementVolume(world, x, y, z, placementScore);
		
		if(result == 0 && world.getBlock(x, y, z).isReplaceableOreGen(world, x, y, z, replacedBlock))
		{
			if(random.nextFloat() < Gases.configurations.world_gasPocketDiabalineChance)
			{
				world.setBlock(x, y, z, Gases.diabalineOre);
			}
			else if(random.nextInt(8) == 0)
			{
				world.setBlock(x, y, z, Gases.ash);
			}
		}
		
		return result;
	}
}