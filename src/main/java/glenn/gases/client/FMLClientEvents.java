package glenn.gases.client;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.world.World;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;

/** Client event handler. Registered in ClientProxy. */
public class FMLClientEvents
{
	/** Handles setting refined diabaline 'glow state'. */
	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
		if(player == null) return;
		
		World world = player.worldObj;
		if(world == null) return;
		
		int x = (int)player.posX;
		int y = (int)player.posY;
		int z = (int)player.posZ;
		
		int radius = 3;
		boolean isSurroundedByGas = false;
		for(int i = -radius; i <= radius; i++)
		{
			for(int j = -radius; j <= radius; j++)
			{
				for(int k = -radius; k <= radius; k++)
				{
					GasType type = GasesFrameworkAPI.getGasType(world, x + i, y + j, z + k);
					if(type != null && type.isIndustrial)
					{
	    	    		isSurroundedByGas = true;
	    	    		break;
					}
				}
			}
		}
		
		Gases.refinedDiabaline.glowState = isSurroundedByGas;
	}
}
