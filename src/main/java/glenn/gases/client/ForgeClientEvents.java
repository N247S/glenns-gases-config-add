package glenn.gases.client;

import glenn.gases.Gases;
import glenn.gasesframework.api.GasesFrameworkAPI;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ForgeClientEvents
{
	private EntityLivingBase previousEventEntity;
	
	@SubscribeEvent
	public void onPlaySoundAtEntity(PlaySoundAtEntityEvent event)
	{
		if(event.entity instanceof EntityLivingBase)
		{
			EntityLivingBase entity = (EntityLivingBase)event.entity;
			if(previousEventEntity != entity)
			{
				World world = event.entity.worldObj;
				
				int x = MathHelper.floor_double(entity.posX);
				int y = MathHelper.floor_double(entity.posY + entity.getEyeHeight());
				int z = MathHelper.floor_double(entity.posZ);
				
				GasType type = GasesFrameworkAPI.getGasType(world, x, y, z);
				if(type == Gases.gasTypeHelium)
				{
					previousEventEntity = entity;
					
					world.playSoundAtEntity(entity, event.name, event.volume, event.pitch * 5.0f);
					event.setCanceled(true);
				}
			}
			else
			{
				previousEventEntity = null;
			}
		}
	}
}
