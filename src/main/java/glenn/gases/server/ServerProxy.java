package glenn.gases.server;

import glenn.gases.common.CommonProxy;

/** Client-side proxy object. */
public class ServerProxy extends CommonProxy
{
	/** Registers client-side event handlers. */
	public void registerEventHandlers()
	{
		super.registerEventHandlers();
	}
}
