package glenn.moddingutils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;
import N2Configuration.api.core.ConfigFile;

public class CoreConfigFile extends ConfigFile
{
	private static final String[] ConfigFileDescription = new String[]
	{
		"",
		"Glenn's Gasses Core ConfigFile",
		"",
	};
	
	private static final String[] CoalSectionDescription = new String[]
	{
		"Here you can add your custom CoalBlocks. These will spawn in CoalGas when broken",
		"(ModID:Block:Metadata)",
	};
	
	private static final String[] CoalSectionDefaultValue = new String[]
	{
		"coal_ore",	
	};

	public CoreConfigFile(String fileName, File targetDirectory)
	{
		super(fileName, targetDirectory);
		this.setCustomSectionStarter(null);
		this.setCustomSectionEnder(null);
		this.setDescription(ConfigFileDescription);
	}

	@Override
	public void generateFile()
	{
		super.addNewStringArraySection("CoalBlockTypes", CoalSectionDescription, CoalSectionDefaultValue, false, true);
	}
	
	public List<ItemStack> getCoalBlockTypes()
	{
		List<String> finalItemStack = null;
		List<ItemStack> ItemStackList = new ArrayList<ItemStack>();

		String[] finalString = (String[]) super.getValue("CoalBlockTypes");

		for(int i1 = 0; i1 < finalString.length; i1++)
		{
			String s1 = finalString[i1];
			Pattern p1 = Pattern.compile(":");
			String[] s2 = p1.split(s1);
			
			for(String s3 : s2)
			{
				finalItemStack = new ArrayList<String>();
				if(s3.length() > 0)
				{
					finalItemStack.add(s3);
				}
			}
			
			Item item = null;
			Block block = null;
			switch(finalItemStack.size())
			{
			case 1:
				if((item = GameRegistry.findItem("minecraft", finalItemStack.get(0))) != null)
					ItemStackList.add(new ItemStack(item));
				else if((block = GameRegistry.findBlock("minecraft", finalItemStack.get(0))) != null)
					ItemStackList.add(new ItemStack(block));
				break;	
			case 2:
				if(!isInteger(finalItemStack.get(1)))
				{
					if((item = GameRegistry.findItem(finalItemStack.get(0), finalItemStack.get(1))) != null)
						ItemStackList.add(new ItemStack(item));
					else if((block = GameRegistry.findBlock(finalItemStack.get(0), finalItemStack.get(1))) != null)
						ItemStackList.add(new ItemStack(block));
				}
				else
				{
					if((item = GameRegistry.findItem("minecraft", finalItemStack.get(0))) != null)
					{
						ItemStack itemStack1 = new ItemStack(item);
						itemStack1.setItemDamage(Integer.parseInt(finalItemStack.get(1)));
						ItemStackList.add(itemStack1);
					}
					else if((block = GameRegistry.findBlock("minecraft", finalItemStack.get(0))) != null)
					{
						ItemStack itemStack1 = new ItemStack(block);
						itemStack1.setItemDamage(Integer.parseInt(finalItemStack.get(1)));
						ItemStackList.add(itemStack1);
					}
				}
				break;	
			case 3:
				if(isInteger(finalItemStack.get(2)))
				{
					if((item = GameRegistry.findItem(finalItemStack.get(0), finalItemStack.get(1))) != null)
					{
						ItemStack itemStack1 = new ItemStack(item);
						itemStack1.setItemDamage(Integer.parseInt(finalItemStack.get(2)));
						ItemStackList.add(itemStack1);
					}
					else if((block = GameRegistry.findBlock(finalItemStack.get(0), finalItemStack.get(1))) != null)
					{
						ItemStack itemStack1 = new ItemStack(block);
						itemStack1.setItemDamage(Integer.parseInt(finalItemStack.get(2)));
						ItemStackList.add(itemStack1);
					}
				}
				break;
			default:
				break;
			}
		}
		if(ItemStackList.isEmpty())
			return null;
		return ItemStackList;
	}

	private static boolean isInteger(String s)
	{
		try
		{
			Integer.parseInt(s);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}
