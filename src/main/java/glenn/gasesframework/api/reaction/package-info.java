/**
*	The Gases Framework API, used to do whatever Glenn's Gases can do, and more.
*	http://www.jamieswhiteshirt.com/minecraft/mods/gases/
*/
@API(apiVersion = GasesFrameworkAPI.VERSION, owner = GasesFrameworkAPI.OWNER, provides = GasesFrameworkAPI.PROVIDES)
package glenn.gasesframework.api.reaction;

import cpw.mods.fml.common.API;
import glenn.gasesframework.api.GasesFrameworkAPI;